package com.example.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telecom.Call.Details
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast

class MainActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    var courses = arrayOf<String?>("Teknologi Informasi", "Teknik Komputer",
        "Teknik Informatika", "Pendidikan Teknologi Informasi",
        "Sistem Informasi")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val spin = findViewById<Spinner>(R.id.spinner)
        spin.onItemSelectedListener = this

        val ad: ArrayAdapter<*> = ArrayAdapter<Any?>(
            this,
            android.R.layout.simple_spinner_item,
            courses)

        ad.setDropDownViewResource(
            android.R.layout.simple_spinner_dropdown_item)

        spin.adapter = ad

        val submit = findViewById<Button>(R.id.submit)
        submit.setOnClickListener{
            val submit = findViewById<Button>(R.id.submit)

            fun isFormValid(): Boolean {
                val nama = findViewById<TextView>(R.id.nama).text.toString()
                val jk = findViewById<RadioGroup>(R.id.radioGroup).checkedRadioButtonId
                val prodi = findViewById<Spinner>(R.id.spinner).selectedItem.toString()

                return if (nama.isNotEmpty() && jk != -1 && prodi.isNotEmpty()) {
                    true
                } else {
                    false
                }
            }

            submit.setOnClickListener{
                if (isFormValid()) {
                    goToSecondActivity()
                } else {
                    Toast.makeText(this, "Data anda belum lengkap", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    override fun onItemSelected(parent: AdapterView<*>?,
                                view: View, position: Int,
                                id: Long) {}

    private fun goToSecondActivity() {
        val intent = Intent(this, result::class.java)

        val nama = findViewById<TextView>(R.id.nama).text.toString()

        val jk = findViewById<RadioGroup>(R.id.radioGroup).checkedRadioButtonId
        val radioButton: RadioButton = findViewById(jk)
        val radioButtonText = radioButton.text.toString()

        val prodi = findViewById<Spinner>(R.id.spinner).selectedItem.toString()

        val bundle = Bundle().apply{
            putString("nama", nama)
            putString("radioButtonText",radioButtonText)
            putString("prodi", prodi)
        }

        intent.putExtra("bundle", bundle)
        startActivity(intent)

    }

    override fun onRestart() {
        super.onRestart()
        findViewById<TextView>(R.id.nama).text = ""
        findViewById<RadioGroup>(R.id.radioGroup).clearCheck()
        findViewById<Spinner>(R.id.spinner).setSelection(0)
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {}


}
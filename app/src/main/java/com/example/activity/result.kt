package com.example.activity

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.RadioGroup
import android.widget.Spinner
import android.widget.TextView

class result : AppCompatActivity() {
    @SuppressLint("WrongViewCast")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result)

        val intent = intent
        if (intent != null) {
            val bundle = intent.getBundleExtra("bundle")
            val textViewValue = bundle?.getString("nama")
            val radioButtonText = bundle?.getString("radioButtonText")
            val spinnerValue = bundle?.getString("prodi")

            val displayNama = findViewById<TextView>(R.id.nama2)
            val displayJk = findViewById<TextView>(R.id.jk2)
            val displayProdi = findViewById<TextView>(R.id.spinner2)
            displayNama.text = textViewValue
            displayJk.text = radioButtonText
            displayProdi.text = spinnerValue
        }

        val backBtn = findViewById<Button>(R.id.backBtn)
        backBtn.setOnClickListener {
            finish()
        }

    }
}